/*
Duel Commander, version 0.2.1.1

Copyright (C) 2009 Tommi Helander
tommi_helander@users.sourceforge.net

This file is part of Duel Commander.

Duel Commander is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Duel Commander is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Duel Commander.  If not, see <http://www.gnu.org/licenses/>.
*/

/* CHARACTER STRUCTURE */
struct Character {
	char name[25];
	int hp;
	int ap;
	int attack1[4];
	int attack2[4];
	int defence1[3];
	int defence2[3];
	float damage_multiplier;
	float accuracy_multiplier;
};
